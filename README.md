#Blue's configuration for linux

This repository contains my configuration files for the following programs:

* neovim
* gdb
* zsh
* i3
* clang-format
* git

#Setup

The following section explains how to install this configuration, be careful not to overwrite your own configuration files !

```
#Clone the repository
git clone https://bitbucket.org/_Blue/config_linux
cd config_linux

#Install required packages (for debian based distributions)
sudo apt-get install `cat packages.txt`

#Run install script
./setup.sh
```

This script will clone, configure, build and install vim.  
It will then apply the configuration files, install vim plugins  
and finally it will setup zsh with [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh).


**Note**: If you're running a debian based distribution, you can simply run: 
```
curl https://config.bluecode.fr | bash
```

#About
You are encouraged to use, modify and redistribute this repository and its content
