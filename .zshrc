export ZSH=~/.oh-my-zsh


export PATH="$PATH:$HOME/.blue/scripts"
ZSH_THEME="robbyrussell"

DISABLE_AUTO_UPDATE="true"
COMPLETION_WAITING_DOTS="true"

plugins=(zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

#Plugins
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)
GIT_PROMPT_EXECUTABLE=haskell
source ~/.oh-my-zsh/custom/plugins/zsh-git-prompt/zshrc.sh #Load git plugin

_hostname=$(hostname --fqdn)
if [ $(id -u) = 0 ]; then
  PROMPT='%{$fg_bold[red]%}root@%{$fg_bold[white]%}'"$_hostname"' ${ret_status}%{$fg[cyan]%}%c%{$reset_color%}$(git_super_status) '
else
  PROMPT='%{$fg_bold[white]%}'"$_hostname $PROMPT"'$(git_super_status) '
fi

#Aliases
alias n='terminator' #Open a new terminal emulator
alias gdb='gdb -q' #Remove gdb startup message
alias m='make'
alias c='clear'

#Fix to use Ctrl - S in vim
alias vim="stty stop '' -ixoff ;vim"
alias v=vim

export EDITOR=vim
setopt autopushd pushdminus pushdsilent pushdtohome # let cd behave as pushd


#Key bindings
_open_nemo() nemo .
zle -N _open_nemo
bindkey '^n' _open_nemo #Bind ctrl-n to open nemo

_open_new_prompt() terminator 2> /dev/null
zle -N _open_new_prompt
bindkey '^[\n' _open_new_prompt #Bind ctrl-alt-return to open ne prompt


_cd_up()
{
  cd ..
  zle reset-prompt
}
zle -N _cd_up
bindkey '^[[1;5A' _cd_up #Bind ctrl-del to move up

_popd()
{
  popd 2> /dev/null > /dev/null
  zle reset-prompt
}
zle -N _popd
bindkey '^[[1;5B'  _popd # Bind ctrl-end to go back


bindkey '^f' history-incremental-search-backward

#Functions
function tag()
{
  if [ $# -eq 0 ]; then
    echo "Missing argument"
    exit
  else
    ~/.blue/scripts/ctags_refresh.sh
    vim -c ":tag $1"
  fi
}
