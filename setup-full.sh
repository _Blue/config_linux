#! /bin/bash

set -ue
shopt -s xpg_echo

RED='\e[31m'
RESET='\e[0m'


# Run a command as root
root_run() #Cmd
{
  if [ `id -u` == "0" ] ; then
    $*
  else # In case sudo is not installed
    sudo $*
  fi
}

# Check if a package is available
check_package() #Package name
{
  if [ `root_run $check_package_cmd "$1" | wc -l` = 0 ]; then
    return 1
  fi

  return 0
}

# Check that a command is available
check_cmd() # Command name
{
  which $1 > /dev/null

  if [ $? != "0" ]; then
    return 1
  fi

  return 0
}


# Entry point
if check_cmd 'apt-get'; then
  install_cmd='apt-get install -y'
  check_package_cmd='apt-get install --dry-run -y'
  required_cmd='apt-get apt-cache'
  root_run apt-get update || true
  root_run apt-get purge -y 'vim*'
else
  install_cmd='pacman -Sy --noconfirm'
  check_package_cmd='pacman -Ss'
  required_cmd='pacman'
fi

for cmd in "$required_cmd" ; do
  if ! check_cmd $cmd; then
    echo "$RED""Missing command: $cmd, are you on debian / archlinux?$RESET"
  fi
done

# Install git
root_run $install_cmd git

# Clone repo
mkdir -p ~/repos
pushd ~/repos > /dev/null


git clone https://bitbucket.org/_Blue/config_linux
cd ~/repos/config_linux

# Install packages

to_install=''

for package in `cat packages.txt`; do

  if check_package "$package"; then
    to_install="$to_install $package"
  else
    echo "$RED""Warning: Missing package: $package $RESET"
  fi
done

root_run "$install_cmd" $to_install

# Run setup script
SHELL=/bin/zsh ./setup.sh

popd

echo 'Done :)'
