#! /bin/bash

set -e
set -u

BLUE='\e[34m'
RED='\e[31m'
GREEN='\e[32m'
RESET='\e[39m'

TMP_DIR=/tmp/blue_config_setup
BASE=`pwd`
FIREFOX_PROFILE_PATH=~/.mozilla/firefox/*.default

function print
{
  echo -e "$BLUE$1$RESET"
}

function test_python_config()
{
  if [ -d "$2" ]; then
    eval PYTHON$1_CONFIG="$2"

    echo "Found configuration in: $2 for python$1"
    return 0
  fi

  return 1
}

function find_python_config()
{
  # python-config may not support --configdir

  config_path=$(python${1}-config  --configdir 2> /dev/null || true)

  version_command=`echo -e "import sys\nprint(str(sys.version_info[0]) + '.' + str(sys.version_info[1]))"`
  version=`python$1 -c "$version_command"`
  config_pathes="/usr/lib/python$version/config-x86_64-linux-gnu \
    /usr/lib/python$version/config \
  /usr/lib/python$version/config-""$version""m-x86_64-linux-gnu \
  $config_path"


  for path in $config_pathes; do
    if test_python_config $1 $path; then
      return
    fi
  done


  echo -e "$RED"Python$1 config path: config_pathes does not exist "$RESET"

  exit 1
}

find_python_config 3

if ! [ -e 'README.md' ]; then
  echo 'This script must be ran from the root of the repository'
  exit 1
fi

if ! [ -z ${var+1}]; then
  1=' ' #Set a default value to $1, (make sure -u won't make the script abort)
fi

function run_root
{
  if [ $(id -u) == '0' ] ; then
    $*
  else
    sudo $*
  fi
}


function setup_vim
{
  # Create work directory and move to it
  mkdir $TMP_DIR -p
  pushd $TMP_DIR > /dev/null
  cd $TMP_DIR

  # Clean any previous build
  rm -rf "$TMP_DIR/vim"

  print "Setting up vim"
  print "\tDownloading vim"
  git clone https://github.com/vim/vim.git --depth=1

  print "\tCompiling vim"
  cd vim
  ./configure --with-features=huge --enable-multibyte --enable-rubyinterp=yes \
    --enable-pythoninterp=yes \
    --enable-python3interp=yes --with-python3-config-dir="$PYTHON3_CONFIG"\
    --enable-cscope --prefix=/usr\
    --with-gui=gtk3\
    --with-x\
    --x-includes=/usr/include/X11/



  make -j4

  print "\tInstalling vim"
  run_root make install -s

  print "\tSetup vim plugins"
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

  mkdir -p ~/.vim/swap
  mkdir -p ~/.vim/undo
  vim -c ':PlugInstall' -c ':q' -c ':q' || true

  popd > /dev/null
}

if [[ "$#" -ge 1 && "$1" == '-nv' ]]; then # Build only neovim
  SetupNeovim
  exit 0
fi

function apply #Filename
{
  dest=${2:-"$HOME/$1"}
  echo -ne "$BASE/$1 -> $dest: "

  #Ignore if already exists
  if [ -e $dest ]; then
    echo -e "$RED"'SKIPPED (Target already exists)'"$RESET"
    return
  fi

  if ! [ -e "$BASE/$1" ]; then
    echo -e "$RED"'ERROR (Source not found)'"$RESET"
    return
  fi

  #Create directory if needed
  mkdir -p ${dest%/*}

  # Create symbolic link
  ln -s $BASE/$1 "$dest"

  echo -e "$GREEN"DONE"$RESET"
}

function setup_git_plugin
{
  if ! [ -d ~/.oh-my-zsh/custom/plugins/zsh-git-prompt ]; then
    git clone https://github.com/olivierverdier/zsh-git-prompt ~/.oh-my-zsh/custom/plugins/zsh-git-prompt --depth=1
  fi

  pushd "$HOME/.oh-my-zsh/custom/plugins/zsh-git-prompt" > /dev/null
  stack setup
  stack build
  stack install
  stack clean
  popd > /dev/null
}

function setup_syntax_plugin()
{
  if ! [ -d  ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting ]; then
    git clone https://github.com/zsh-users/zsh-syntax-highlighting ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting --depth=1
  fi
}

function setup_firefox()
{
  profile=$(echo $FIREFOX_PROFILE_PATH) # Try to expand path

  if [ "$profile" == "$FIREFOX_PROFILE_PATH" ]; then
    echo "WARNING: Firefox profile not found" # If expansion failed, no profile found
    return 0
  fi

  apply firefox/user.js "$profile/user.js"
}

function setup_oh_my_zsh()
{
  SHELL=/bin/zsh

  if ! [ -d ~/.oh-my-zsh ]; then
    print "Setting up oh-my-zsh"
    git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh --depth=1
  else
    print "Skipped oh-my-zsh"
  fi
}

function setup_diff_so_fancy()
{
  if ! [ -d ~/.oh-my-zsh/custom/diff-so-fancy ]; then
    git clone https://github.com/so-fancy/diff-so-fancy ~/.oh-my-zsh/custom/diff-so-fancy --depth=1
    run_root ln -s "$HOME"/.oh-my-zsh/custom/diff-so-fancy/diff-so-fancy /usr/bin/diff-so-fancy
  fi
}


# Create symbolic links
while read file; do
  apply "$file"
done <files.txt

setup_firefox
setup_vim

#Build zsh
print "Setting up zsh"
setup_oh_my_zsh
setup_git_plugin
setup_syntax_plugin
setup_diff_so_fancy

apply '.oh-my-zsh/custom/themes/robbyrussell.zsh-theme'

run_root chsh -s /bin/zsh $(whoami) # Use sudo to avoid asking for password one more time
