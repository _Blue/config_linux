"
"
"Blue, 2015
"You are free to use, redistribute and modify this file as you wish

"Init
set nocompatible "Disable vi retro-compatibility
filetype off "Disable legacy file type detection

"Plugins init
if ! exists("g:blue_disable_plugins")
  call plug#begin()
  Plug 'vim-scripts/Cpp11-Syntax-Support'
  Plug 'octol/vim-cpp-enhanced-highlight' "Improve C++11 syntax highliting
  Plug 'Valloric/YouCompleteMe', {'do': 'python2 install.py --clang-completer'} "Auto completion
  Plug 'scrooloose/syntastic' "Code analysis
  Plug 'rhysd/vim-clang-format' "Code formatter
  Plug 'nanotech/jellybeans.vim' "Color scheme
  Plug 'xuqix/h2cppx', {'do': 'git checkout vim-port'} "C++ source skeleton generator
  Plug 'kien/rainbow_parentheses.vim' "lisp parenthesis color
  Plug 'neovimhaskell/haskell-vim' "haskell syntax highlight
  Plug 'vim-airline/vim-airline' "Improve status bar
  Plug 'vim-airline/vim-airline-themes'
  Plug 'airblade/vim-gitgutter' " Show file changes
  Plug 'xuhdev/vim-latex-live-preview' "Markdown real time rendering

  call plug#end()
endif

"Basic configuration
syntax on "Allow syntax highlight
filetype plugin indent on
if ! exists("g:blue_disable_plugins")
  colorscheme jellybeans
endif

set nu
set autoread
set colorcolumn=80
set tabstop=2 shiftwidth=2 expandtab
set encoding=utf8
set backspace=indent,eol,start
set mouse=a
set title "Tell vim to set the terminal title according to the file name
set autoindent
set smartindent
set ttimeoutlen=10
set cursorline "Highlight current line
set shell=zsh
set list "Show invisible chars
set listchars=tab:▸\ ,extends:❯,precedes:❮
set t_Co=256      "Use 256 colors
set showbreak=↪ "Show line returns
set bs=indent,eol,start "Improve back behavior
set swapfile
set dir=~/.config/nvim/swap "Put swap files in fixed place
set undofile
set undodir=~/.config/nvim/undo "Put undo files in fixed place
set laststatus=2 " Show status bar
set omnifunc=syntaxcomplete#Complete "default completion mode
set hlsearch "highlight all matches when searching for text
set incsearch "Incremental search mode
set ignorecase smartcase "Be case sensitive only if the pattern contains upper-case letters
set splitbelow "Show preview window below status bar
set noshowmode "Hide current mode, vim-airline already does it
set esckeys
set scrolloff=4 "Start scrolling when we're 4 lines away from margins
set shortmess+=I "Disable starting message
set complete+=kspell "Use dictionary words for completion

if ! exists("g:blue_disable_plugins")
  set completeopt=longest,menu "default completion mode, disabled
endif



"File types
augroup auto_run
  autocmd!

  "Enable tabulation for Makefiles
  autocmd FileType make setlocal ts=4 sts=4 sw=4 noexpandtab
  autocmd BufRead,BufNewFile *.{md,markdown,mdown} set ft=markdown
  autocmd Filetype markdown setlocal spell
  autocmd Filetype markdown setlocal textwidth=90000
  autocmd filetype gitcommit setlocal textwidth=72
  autocmd filetype gitcommit setlocal spell
  autocmd filetype tex set noswapfile "Swap file seems to conflicts with the rendering plugin
  autocmd filetype lisp RainbowParenthesesToggle

  "Restore previous cursor position
  autocmd BufWinEnter * call ResCur()

  "Enable Clang-Format for formating C / CPP files
  autocmd filetype c inoremap <C-K> <C-O>:call Format()<CR>
  autocmd filetype cpp inoremap <C-K> <C-O>:call Format()<CR>
augroup END

"Highlight trailing spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/


"Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
" "100 :  will save up to 100 lines for each register
" "  :20  :  up to 20 lines of command-line history will be remembered
" "  %    :  saves and restores the buffer list
" "  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo

"Helpers
"Restore the cursor position
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

let g:blue_file_pair = { ".h": [".cpp", ".c", ".cc", ".hxx"],                 ".c": [".h"],                 ".cpp": [".hh", ".hpp", ".h"],                 ".hpp": [".cpp"],                 ".hh": [".cc", ".hxx"],                 ".hxx": [".hh", ".h"],                 ".cc": [".hh"]}

"Find position of last '.' in file name, 'foo.txt' -> 3
function! FileSep (filename)
  let current = match(a:filename, "\\.")

  while current != -1 "Find last occurence of '.'
    let value = current
    let current = match(a:filename, "\\.", value + 1)
  endwhile

  return value
endfunction


"Function to switch between a C header and source file
function! Switch ()

  "First save current file
  if &mod
    execute ":w"
  endif

  "Split file name and file extension
  let sep = FileSep(@%)
  let prefix = @%[:sep - 1]
  let suffix = @%[sep :]

  "Check if we are already in a pair file (in this case just jump back)
  if exists("g:blue_previous_file") && g:blue_previous_file[:sep - 1] == prefix  &&g:blue_previous_file[sep :] != suffix
    let filename = g:blue_previous_file
    let g:blue_previous_file = @%
    let fullcmd = "e ". filename
    execute fullcmd
    return
  endif

  let g:blue_previous_file = @%

  "Search for this extension in our dictionnary
  if !has_key(g:blue_file_pair, suffix)
    echoerr "Unknow file type (" . suffix . ")"
    return
  endif

  "Loop through the list
  for ext in g:blue_file_pair[suffix]
    if (filereadable(prefix . ext))
      execute "e ". prefix . ext
      return
    endif
  endfor

  "If not found, fall back on he first item (create a new file)
  execute "e " . prefix . g:blue_file_pair[suffix][0]
endfunction

"Call clang-format and keep current view
function! Format()
  execute "let b:PlugView=winsaveview()"
  execute "ClangFormat"
  execute "call winrestview(b:PlugView)"
endfunction

"Plugin configuration

if ! exists("g:blue_disable_plugins")
  "Syntastic
  set statusline+=%#warningmsg#
  set statusline+=%{SyntasticStatuslineFlag()}
  set statusline+=%*
  let g:syntastic_always_populate_loc_list = 1
  let g:syntastic_auto_loc_list = 0
  let g:syntastic_c_config_file = ".syntastic_c_config"
  let g:syntastic_check_on_open = 0
  let g:syntastic_check_on_wq = 0
  let g:syntastic_enable_highlighting = 1
  let g:Syntastic_c_checkers = ["gcc"]
  let g:syntastic_cpp_config_file = ".syntastic_cpp_config"
  let g:syntastic_cpp_compiler_options = "-std=c++14 -Wall -Wextra"
  let g:syntastic_cpp_check_header = 1
  let g:syntastic_enable_ballons=has('ballon_eval')
  let g:syntastic_warning_symbol='⚠'
  let g:syntastic_error_symbol='✗'
  let g:syntastic_quiet_messages = { "regex": "#pragma once in main file"} " Don't trigger an error for a pragma in a header
  let g:syntastic_python_python_exec = 'python3'

  function! ToggleErrors()
    let old_last_winnr = winnr('$')
    lclose
    if old_last_winnr == winnr('$')
      " Nothing was closed, open syntastic error location panel
      Errors
    endif
  endfunction

  "Map F2 to toggle error window
  imap <silent> <F2> <C-O>:call ToggleErrors()<CR>

  "Airline
  let g:airline_powerline_fonts = 1
  let g:airline_theme='powerlineish'
  let g:airline_exclude_preview=1
  let g:airline_right_sep=''
  let g:airline_left_sep=''

  "YouCompleteMe
  let g:ycm_extra_conf_globlist = ['~/*']
  let g:ycm_filetype_whitelist = { 'cpp': 1, 'c': 1 , 'h': 1, 'hh': 1, 'cc': 1, 'hxx': 1}
  let g:ycm_semantic_triggers = {'cpp': ['re!(?=[a-zA-Z_]{3})'] }
  let g:ycm_show_diagnostics_ui = 0 "Don't conflict with Syntastic
  let g:ycm_autoclose_preview_window_after_insertion = 1


  set completeopt-=preview "enable preview window

  imap <F11> <C-o>:YcmCompleter GoTo<CR>
  imap <S-F11> <C-o>:YcmCompleter GoToDeclaration<CR>
  imap <C-F11> <C-o><C-O>

  "CTags
  "Bind F11 to jump to symbol
  imap <F12> <C-o><C-]>

  "Bind alt-F12 to show list of symbols
  imap <M-F12> <C-o>g]

  "Bind ctrl-F12 to jump back
  imap <C-F12> <C-o><C-T>

  "Bind ctrl-shift-F12 to update tags
  imap <C-S-F12> <C-O>:!~/.blue/scripts/ctags_refresh.sh<CR>

  "Bind shift-F12 to search for a tag
  imap <S-F12> <C-O>:tag<Space>

  "Tell vim to search for ctags file in the file's directory
  set tags=./tags;

  "clang-format
  let g:clang_format#command = "clang-format-3.7"

endif

"Wildmenu completion
set wildmenu "Command line completion
set wildmode=list:longest,full "Show a list matching longest first
set wildignorecase "Ignore case while searching for a match
set wildignore+="*.git"
set wildignore+="*.swp,*.out,*.o,*.a"


"Key mappings
"Remap Ctrl - S to save
inoremap <silent>  <C-S> <C-O>:w<CR>
nnoremap <silent>  <C-S> :w<CR>

"Remap Ctrl - Z to undo
noremap  <C-z> u
inoremap <C-z> <Esc>ui

"And Ctrl - Y to redo
noremap <C-y> <C-r>
inoremap <C-y> <Esc><C-r>i

"Use Ctrl - D to duplicate line
nmap <C-d> mzyyp`z
inoremap <C-d> <Esc>mzyyp`zi

"Classic Ctrl - C, X, and V use
vnoremap <C-c> y
inoremap <C-c> <C-o>Vy<End>
vnoremap <C-x> x
inoremap <C-v> <Esc>vpi
vnoremap <C-v> p

"Bind Ctrl - Space to trigger auto completion
inoremap <Nul> <C-n>

"Bind Ctrl - K to indent without losing cursor's position and scrolling
imap <C-k> <Esc>:let b:PlugView=winsaveview()<CR>gg=G:call winrestview(b:PlugView) <CR>i
map <C-k> :let b:PlugView=winsaveview()<CR>gg=G:call winrestview(b:PlugView) <CR>


"Bind Ctrl-Begin and Ctrl-End to jump fo file begin - end
imap <C-Home> <C-O>gg
imap <C-End> <C-O><S-G>

"Bind Ctrl-backspace to delete previous word
noremap! <C-BS> <C-w>

"Bind Ctrl-L to delete line
imap <C-L> <C-O>dd

"Bind Ctrl-H to switch from header-sources
imap <C-H> <C-O>:call Switch()<CR>

"Bind Ctrl-T to remove indent
imap <C-T> <C-O><<

"Bind ctrl-E to replace word by best match, move the cursor at the end of
"the word
imap <C-E> <C-O>z=1<CR><C-O>e

"Switch between tabs
imap <S-Left> <C-O>:tabp<CR>
imap <S-Right> <C-O>:tabn<CR>
imap <F10> <C-O>:tabnew 

if  exists("g:blue_disable_plugins")
  "status line without plugin,
  set statusline = "%f"

  function! InsertStatuslineColor(mode)
    if a:mode == 'i'
      hi statusline guibg=DarkGreen ctermfg=22 guifg=Black ctermbg=0
    elseif a:mode == 'r'
      hi statusline guibg=Purple ctermfg=5 guifg=Black ctermbg=0
    else
      hi statusline guibg=DarkRed ctermfg=1 guifg=Black ctermbg=0
    endif
  endfunction

  au InsertEnter * call InsertStatuslineColor(v:insertmode)
  au InsertLeave * hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=15
endif


