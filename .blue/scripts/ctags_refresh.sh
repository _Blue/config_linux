#! /bin/bash

#Script to update the CTags symbol file
#You are free to use, redistribute and modify this file as you wish
#Blue, 2015

FILENAME=tags
function update_file()
{
  echo "updating in dir $PWD"
 
  ctags --c-kinds=+px -R  . #Update index for headers and source files (+px)
  exit 0
}

function upsearch() 
{
    test / == "$PWD" && return || test -e "$1" && update_file && return || cd .. && upsearch "$1"
}


upsearch $FILENAME

echo "$FILENAME not found"

exit 1
