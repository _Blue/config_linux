#! /bin/bash

# Script to change the name of the AUTHOR of all commits
# To be used with a git repository
# WARNING: Use with caution, you will have to push --force all your branches
# `push -f', may overwrite commits, be sure to be up to date
#
# Inspired from work of Moray Baruh
# Blue, 2016
set -e
set -u

export OLDNAME='old_name_here'
export NEWNAME='new_name_here'
export EMAIL='new_email_here'

branches="master dev"

for branch in $branches
do
  git pull origin $branch
  git checkout $branch

  git filter-branch -f --env-filter 'if [ "$GIT_AUTHOR_NAME" = "$OLDNAME" ]; then
  GIT_AUTHOR_EMAIL="$EMAIL";
  GIT_AUTHOR_NAME=$NEWNAME;
  GIT_COMMITTER_EMAIL=$GIT_AUTHOR_EMAIL;
  GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"; fi' -- --all

done
