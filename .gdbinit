# Configuration file for gdb
# You are free to use, modify and redistribute this file as you wish
# Blue, 2015

set history filename ~/.gdb_history
set history save on
set prompt (gdb)

#Disable confirmation when exiting or restarting the program
set confirm off

#Bind z to enable graphical mode
define z
  layout src
end

#Bind zz to disable graphical mode
define zz
  tui disable
end


#ff will change the focus in layout mode
define ff
  focus cmd
end

define fg
  focus src
end

#define f to return from current function
define f
  finish
end

#Set follow mode when a fork occurs
define follow-child
  echo following child \n
  set follow-fork-mode child
end

define follow-parent
  echo following parent \n
  set follow-fork-mode parent
end
